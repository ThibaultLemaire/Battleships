# Battleships

A very basic implementation of the [Battleship game] to be run on the CLI.

[Battleship game]: https://en.wikipedia.org/wiki/Battleship_(game)

---

⚠ **ATTENTION** : This is a school project hosted here for demonstration purposes only. It is not fit for any real-world scenario.

---

![screenshot](Screenshot_20200119_190846.png)

## How to play

1. Install [Nix].
2. Run main

   ``` sh
   ./Main.py
   ```

[Nix]: https://nixos.org/nix/
