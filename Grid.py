import random

from Cell import Cell

class Grid:
    def __init__(self, width = 10, height = 10):
        self.__ships = []
        self.__generate_grid(width, height)

    def __generate_grid(self, width, height):
        grid = []
        for i in range(height):
            grid.append([])
            for j in range(width):
                new_cell = Cell()
                grid[i].append(new_cell)
                
                if i > 0:
                    new_cell.set_above(grid[i-1][j])
                    grid[i-1][j].set_below(new_cell)

                if j > 0:
                    new_cell.set_left(grid[i][j-1])
                    grid[i][j-1].set_right(new_cell)
                

        self.set_grid(grid)
    
    def get_grid(self):
        return self.__grid

    def set_grid(self, value):
        self.__grid = value

    def get_cell(self, x, y):
        return self.get_grid()[y][x]

    def set_cell(self, x, y, value):
        self.get_grid()[y][x].set_ship(value)

    def set_multiple_cells(self, coordinates_set, value):
        for coordinates in coordinates_set:
            self.set_cell(coordinates[0],
                          coordinates[1],
                          value)
    def get_ships(self):
        return self.__ships

    def get_free_cells(self):
        free_cells = []
        for row in self.get_grid():
            for cell in row:
                if cell.is_free():
                    free_cells.append(cell)
        return free_cells

    def get_player_view(self):
        representation = []
        for row in self.get_grid():
            row = [str(cell) for cell in row]
            representation.append(" ".join(row))
        return representation

    def get_enemy_view(self):
        representation = []
        for row in self.get_grid():
            row = [cell.get_value_simplified() for cell in row]
            representation.append(" ".join(row))
        return representation
                

    def __select_cells(self, cell, orientation, length):
        if orientation == "horizontal":
            return cell.propagate_horizontal(length)
        elif orientation == "vertical":
            return cell.propagate_vertical(length)

    def __place_ship(self, ship):
        available_cells = self.get_free_cells()
        selected_cells = []
        center = None
        while selected_cells == []:
            center = random.choice(available_cells)
            available_cells.remove(center)
            if available_cells == []:
                raise Exception("Unable to place ship: no room available")
            selected_cells = self.__select_cells(center,
                                                 ship.get_orientation(),
                                                 ship.get_length())
            if selected_cells == []:
                ship.switch_orientation()
                selected_cells = self.__select_cells(center,
                                                     ship.get_orientation(),
                                                     ship.get_length())
        ship.set_center(center)
        ship.set_cells(selected_cells)
        

    def add_ship(self, ship):
        self.__place_ship(ship)
        self.__ships.append(ship)

    def receive_shot(self, x, y):
        cell = self.get_cell(x,y)
        if not cell.is_shot():
            cell.receive_shot()

    def has_lost(self):
        for ship in self.__ships:
            if not ship.is_destroyed():
                return False

        return True

