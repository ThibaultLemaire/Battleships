import random

class Ship:
    _tag = "§"
    def __init__(self, length = 3):
        self.__center = None
        self.__cells = []
        self.__length = length
        self.__health = self.__length
        self.__orientation = random.choice([True, False])

    def set_center(self, value):
        self.__center = value

    def set_cells(self, cells):
        self.__cells = cells
        for cell in cells:
            cell.set_ship(self)
            

    def get_length(self):
        return self.__length

    def get_orientation(self):
        if self.__orientation:
            return "horizontal"
        else:
            return "vertical"

    def switch_orientation(self):
        self.__orientation = not self.__orientation
            
    def get_tag(self):
        return self._tag

    def receive_shot(self):
        self.__health -= 1

    def is_destroyed(self):
        return self.__health == 0
