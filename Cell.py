class Cell:
    __default = "."
    __water = "W"
    __shot = "X"
    def __init__(self):
        self.__ship = None
        self.__is_shot = False
        self.__above_neighbour = None
        self.__below_neighbour = None
        self.__left_neighbour = None
        self.__right_neighbour = None

    def is_shot(self):
        return self.__is_shot

    def get_ship(self):
        return self.__ship

    def set_ship(self, value):
        self.__ship = value

    def is_free(self):
        return self.get_ship() is None and not self.is_shot()

    def get_value(self):
        if self.get_ship() is None:
            if self.is_shot():
                return self.__class__.__water
            else:
                return self.__class__.__default
        else:
            value = self.get_ship().get_tag()
            if self.is_shot():
                value = value.lower()
            return value

    def get_value_simplified(self):
        if self.is_shot():
            if self.get_ship() is None:
                return self.__class__.__water
            else:
                return self.__class__.__shot
        else:
            return self.__class__.__default

    def get_above(self):
        return self.__above_neighbour
    
    def set_above(self, value):
        self.__above_neighbour = value

    def get_below(self):
        return self.__below_neighbour

    def set_below(self, value):
        self.__below_neighbour = value

    def get_left(self):
        return self.__left_neighbour

    def set_left(self, value):
        self.__left_neighbour = value

    def get_right(self):
        return self.__right_neighbour

    def set_right(self, value):
        self.__right_neighbour = value

    def propagate_vertical(self, length):
        top_cell = self
        bottom_cell = self
        selected_cells = [self]
        switch = True
        for i in range(length-1):
            if switch:
                top_cell = top_cell.get_above()
                if top_cell is not None and top_cell.is_free():
                    selected_cells.append(top_cell)
                else:
                    return []
            else:
                bottom_cell = bottom_cell.get_below()
                if bottom_cell is not None and bottom_cell.is_free():
                    selected_cells.append(bottom_cell)
                else:
                    return []
            switch = not switch
        return selected_cells

    def propagate_horizontal(self, length):
        leftmost_cell = self
        rightmost_cell = self
        selected_cells = [self]
        switch = True
        for i in range(length-1):
            if switch:
                leftmost_cell = leftmost_cell.get_left()
                if leftmost_cell is not None and leftmost_cell.is_free():
                    selected_cells.append(leftmost_cell)
                else:
                    return []
            else:
                rightmost_cell = rightmost_cell.get_right()
                if rightmost_cell is not None and rightmost_cell.is_free():
                    selected_cells.append(rightmost_cell)
                else:
                    return []
            switch = not switch
        return selected_cells

    def receive_shot(self):
        self.__is_shot = True
        if self.get_ship() is not None:
            self.get_ship().receive_shot()
    
    def __str__(self):
        return str(self.get_value())
