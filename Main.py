#! /usr/bin/env nix-shell
#! nix-shell -p python3 -i python

from Grid import Grid
from AircraftCarrier import AircraftCarrier
from Battleship import Battleship
from Destroyer import Destroyer
from Submarine import Submarine
from Torpedo import Torpedo

def show_grid(player_grid, enemy_grid):
    player_grid = player_grid.get_player_view()
    enemy_view = enemy_grid.get_enemy_view()
    for i in range(len(player_grid)):
        print(player_grid[i] + "\t" + str(i) + " " + enemy_view[i])

def new_grid(width, height):
    grid = Grid(width, height)
    grid.add_ship(AircraftCarrier())
    grid.add_ship(Battleship())
    grid.add_ship(Destroyer())
    grid.add_ship(Submarine())
    grid.add_ship(Torpedo())
    return grid
gameon = True
switch = True
width = 10
height = 10
grid1 = new_grid(width, height)
grid2 = new_grid(width, height)
while gameon:
    print("\t\t\t  0 1 2 3 4 5 6 7 8 9")
    if switch:
        show_grid(grid1, grid2)
        print("Player 1's turn:")
        x = int(input("Column (0-{0}) : ".format(width-1)))
        y = int(input("Line (0-{0}) : ".format(height-1)))
        grid2.receive_shot(x, y)
        if grid2.has_lost():
            print("Player 1 wins!")
            gameon = False
    else:
        show_grid(grid2, grid1)
        print("Player 2's turn:")
        x = int(input("Column (0-{0}) : ".format(width-1)))
        y = int(input("Line (0-{0}) : ".format(height-1)))
        grid1.receive_shot(x, y)
        if grid1.has_lost():
            print("Player 2 wins!")
            gameon = False
    
    switch = not switch
